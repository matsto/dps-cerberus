build:
	docker-compose build --parallel --pull

up: build
	docker-compose up

down:
	docker-compose down

stop:
	docker-compose stop

docker-clean:
	yes|docker system prune

docker-clean-all:
	yes|docker system prune -a

ps:
	docker ps

volumes-ls:
	docker volume ls

restart: down up
