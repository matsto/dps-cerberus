from datetime import datetime
from typing import Union


def print_msg(running_yanai_build: Union[int, str], msg: str) -> None:
    running_yanai_build = str(running_yanai_build)
    now = datetime.now()
    running_build = running_yanai_build
    print(f"{now} - {running_build} - {msg}")
