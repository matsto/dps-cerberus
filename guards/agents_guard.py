import re

from guards.base_guard import BaseGuard
from utils import print_msg


class AgentsGuard(BaseGuard):
    def _check_agents_prepare(self) -> bool:
        _exit_code_regex = re.compile(r"script returned exit code \d")
        _disconnect_regex = re.compile(r"Cannot contact EC2 (elastic-ci)")

        if _disconnect_regex.search(
            self.console_output
        ) or _exit_code_regex.search(self.console_output):
            print_msg(self.running_yanai_build, "PREPARE AGENT FAILED")
            return True
        return False

    def validate(self) -> bool:
        return self._check_agents_prepare()
