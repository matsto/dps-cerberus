import re

from guards.base_guard import BaseGuard
from utils import print_msg


class TestsGuard(BaseGuard):
    def check_integration_test(self) -> bool:
        _err_regex = re.compile(r"Captured stdout:")
        if _err_regex.search(self.console_output):
            print_msg(
                self.running_yanai_build,
                "E2E or local integration tests failed",
            )
            return True
        return False

    def validate(self) -> bool:
        return self.check_integration_test()
