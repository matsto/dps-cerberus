import re

from guards.base_guard import BaseGuard
from utils import print_msg


class RemovedNodeGuard(BaseGuard):
    def removed_node_test(self) -> bool:
        _err_regex = re.compile(r"was marked offline: Node is being removed")
        if _err_regex.search(self.console_output):
            print_msg(
                self.running_yanai_build,
                "Node has been removed",
            )
            return True
        return False

    def validate(self) -> bool:
        return self.removed_node_test()
