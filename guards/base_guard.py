from abc import ABC, abstractmethod

from jenkins import Jenkins


class BaseGuard(ABC):
    def __init__(
        self, server: Jenkins, running_yanai_build: int, console_output: str
    ) -> None:
        self.server = server
        self.running_yanai_build = running_yanai_build
        self.console_output = console_output

    @abstractmethod
    def validate(self) -> bool:
        raise NotImplementedError
