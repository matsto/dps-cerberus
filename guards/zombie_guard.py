from datetime import datetime, timedelta

from guards.base_guard import BaseGuard
from utils import print_msg


class ZombieGuard(BaseGuard):
    def _check_zombie(self):
        build_info = self.server.get_build_info(
            "yanai-build", self.running_yanai_build
        )
        start_timestamp = datetime.fromtimestamp(
            build_info["timestamp"] / 10 ** 3
        )
        deadline = start_timestamp + timedelta(hours=3, minutes=50)
        now = datetime.now()
        if deadline < now:
            print_msg(self.running_yanai_build, "ZOMBIE")
            return True
        return False

    def validate(self) -> bool:
        return self._check_zombie()
