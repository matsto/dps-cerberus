import re

from guards.base_guard import BaseGuard
from utils import print_msg


class FailedJobGuard(BaseGuard):
    def removed_node_test(self) -> bool:
        _err_regex = re.compile(r"Finished: FAILURE")
        if _err_regex.search(self.console_output):
            print_msg(
                self.running_yanai_build,
                "Job failed",
            )
            return True
        return False

    def validate(self) -> bool:
        return self.removed_node_test()
