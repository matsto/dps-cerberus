import os

import jenkins

from settings.settings import JENKINS_PASSWORD, JENKINS_URL, JENKINS_USER

server = jenkins.Jenkins(
    os.environ.get("JENKINS_URL", JENKINS_URL),
    username=os.environ.get("JENKINS_USER", JENKINS_USER),
    password=os.environ.get("JENKINS_PASSWORD", JENKINS_PASSWORD),
    timeout=30,
)
