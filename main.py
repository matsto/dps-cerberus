from cerberus.cerberus import cerberus
from guards.agents_guard import AgentsGuard
from guards.failed_job import FailedJobGuard
from guards.removed_node_guard import RemovedNodeGuard
from guards.tests_guard import TestsGuard
from guards.zombie_guard import ZombieGuard

if __name__ == "__main__":
    cerberus.register_guard(RemovedNodeGuard)
    cerberus.register_guard(AgentsGuard)
    cerberus.register_guard(TestsGuard)
    cerberus.register_guard(ZombieGuard)
    cerberus.register_guard(FailedJobGuard)

    cerberus.execute()
