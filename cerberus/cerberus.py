import concurrent.futures
from time import sleep
from typing import List, Optional

from guards.base_guard import BaseGuard
from jenkins_session.jenkins_session import server
from utils import print_msg


class Cerberus:
    def __init__(self, server):
        self._server = server
        self._running_builds = []
        self._running_yanai_build = 0
        self._guards: List[BaseGuard] = []
        self._console_output: Optional[str] = None

    def register_guard(self, guard: BaseGuard) -> None:
        self._guards.append(guard)

    def _get_running_builds(self):
        while True:
            try:
                self._running_builds = self._server.get_running_builds()
            except Exception:
                sleep(60)
            else:
                break

    def _get_running_builds_numbers(self) -> None:
        for build in self._running_builds:
            if build["name"] == "yanai-build":
                self._running_yanai_build = build["number"]
                break
            self._running_yanai_build = 0

    def _get_console_output(self):
        self._console_output = self._server.get_build_console_output(
            "yanai-build", self._running_yanai_build
        )

    def _stop_current_build(self):
        print_msg(self._running_yanai_build, "stoping build")
        self._server.stop_build("yanai-build", self._running_yanai_build)
        sleep(300)

    def _refresh(self):
        self._get_running_builds()
        self._get_running_builds_numbers()
        if self._running_yanai_build:
            self._get_console_output()

    def _run_validators(self) -> bool:
        with concurrent.futures.ThreadPoolExecutor(
            max_workers=len(self._guards)
        ) as executor:
            futures = {
                executor.submit(
                    gd(
                        self._server,
                        self._running_yanai_build,
                        self._console_output,
                    ).validate
                )
                for gd in self._guards
            }
        for f in concurrent.futures.as_completed(futures):
            try:
                _result = f.result()
            except Exception as ex:
                print(ex)
            else:
                if _result:
                    return True
        return False

    def execute(self):
        while True:
            self._refresh()
            if self._running_yanai_build:
                has_errors = self._run_validators()
                if has_errors:
                    # self._stop_current_build()
                    sleep(120)
            else:
                print_msg("IDLE", "No running builds")
                sleep(60)
            sleep(60)


cerberus = Cerberus(server)
